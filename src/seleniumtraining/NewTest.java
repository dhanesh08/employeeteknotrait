package seleniumtraining;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

public class NewTest {
	WebDriver driver;
  @BeforeTest
  public void launch()
  {
	    System.setProperty("webdriver.gecko.driver","C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\geckodriver.exe");
	    driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		
  }
  @Test(dataProvider="Teknotrait")
  public void input(String username, String password) throws Exception 
  {
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);
	  driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys(password);
	  driver.findElement(By.xpath("//button[@type='submit']")).click();
	  
  }
  
  @DataProvider(name="Teknotrait")
  public Object[][] passData()
  {
	  Object[][] data = new Object[2][2];
	  data[0][0]="dhaneshbhat61@gmail.com";
	  data[0][1]="1234567890";
	  data[1][0]="dhanesh_bhat@yahoo.com";
	  data[1][1]="bestintheworld081996";
	  return data;
  }
  
   
}
