package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Attendease {
	static WebDriver driver;
	@BeforeTest
	public static void enterURL()
	{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\geckodriver.exe");
		driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://bu-event-setup-2.preview.attendease.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	@Test
	public static void enterDetails() throws InterruptedException
	{
		driver.findElement(By.xpath("//div[@id='attendease-navbar-collapse']//span[contains(text(),'Register')]")).click();
		driver.findElement(By.xpath("//label[contains(text(),'All groups: paid primary pass')]")).click();
		driver.findElement(By.id("continue-btn")).click();
		driver.findElement(By.id("builtin-fname")).sendKeys("abhi");
		driver.findElement(By.id("builtin-lname")).sendKeys("sen");
		driver.findElement(By.id("builtin-email")).sendKeys("abhi+5081@attendease.com");
		driver.findElement(By.id("builtin-password")).sendKeys("1234567890");
		Thread.sleep(7000);
		driver.findElement(By.id("consent-notice-0")).click();
		Thread.sleep(7000);
		driver.findElement(By.xpath("//button[@id='next-step-btn']")).click();
		Thread.sleep(7000);
		driver.switchTo().frame(driver.findElement(By.name("__privateStripeFrame8")));
		driver.findElement(By.xpath("//input[@name='cardnumber']")).sendKeys("123456789012");
		driver.findElement(By.name("exp-date")).sendKeys("1126");
		driver.findElement(By.name("cvc"));
		List<WebElement> elements = driver.findElements(By.tagName("iframe"));
		int numberOfTags=elements.size();
		System.out.println("No of frames : "+numberOfTags);


	}
	@AfterTest
	public static void close() throws Exception
	{
		Thread.sleep(5000);
		driver.quit();
	}

}
