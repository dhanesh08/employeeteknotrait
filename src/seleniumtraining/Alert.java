package seleniumtraining;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Alert {
		static WebDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver","C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\geckodriver.exe");
	    driver=new FirefoxDriver();
		driver.manage().window().maximize();
		
		// Alert
		driver.switchTo().alert().accept();
		driver.switchTo().alert().dismiss();
		driver.switchTo().alert().getText();

	}

}
