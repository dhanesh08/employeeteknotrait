package seleniumtraining;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EmployeeTeknotraitAddTimesheet
{
	static WebDriver driver;
	@BeforeTest
	public static void login() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");

		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}
	
	@Test
	public static void typeAddTimesheet() throws Exception
	{
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]")).click();
		// Click on Add timesheet
		driver.findElement(By.id("add_timesheet")).click();
	}
	
	@AfterTest
	public static void logout() throws Exception
	{
		Thread.sleep(3000);
		driver.quit();
	}
}
