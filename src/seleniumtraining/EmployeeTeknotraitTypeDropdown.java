package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
/*
 Test data:- Type Dropdown
 */
public class EmployeeTeknotraitTypeDropdown {
	static WebDriver driver;
	@BeforeTest
	public static void login() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");

		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}
	@Test
	public static void testtypeDropdown() throws Exception {
		// Click on Employee timesheet button
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]"))
				.click();

		Thread.sleep(5000);
		Select type_dd = new Select(driver.findElement(By.xpath("//select[@class='work_type']")));
		String type_text="Normal Hours";
		type_dd.selectByVisibleText(type_text);
		// Verification
		List<WebElement> options=type_dd.getOptions();
		for (WebElement type:options)
		{
			if (type.getText().contains(type_text))
			{
			System.out.println(type.getText());
			}
		}

		// Click on filter button
		driver.findElement(By.id("filter")).click();
		System.out.println("The filter button is clicked!");
		Thread.sleep(4000);
	}
	@AfterTest
	public static void logout() throws InterruptedException
	{
		Thread.sleep(4000);
		driver.quit();
		
	}

}
