package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EmployeeTeknotrait {
	static WebDriver driver;

	@BeforeTest
	public static void login() {
		
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}

	/*
	 Test Data:-
	 User dropdown
	 Project dropdown
	 Type dropdown
	 status dropdown
	 Filter button
	 clear button
	 
	  
	 */
	@Test
	public void f() throws Exception {
		System.out.println("called !");

		// Click on Employee timesheet button
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]"))
				.click();

		// Enter all the details
		Select user_dd = new Select(driver.findElement(By.id("filteruser")));
		// Verification
		List <WebElement> options= user_dd.getOptions();
		for (WebElement user:options)
		{
			System.out.println(user.getText());
		}
		user_dd.selectByVisibleText("Dharmendra Patel");
		
		
		
		
		
		
		
		System.out.println("All user details entered!");

		// Click on filter button
		driver.findElement(By.id("filter")).click();
		System.out.println("The filter button is clicked!");
		Thread.sleep(4000);

		// Verification
		
		// Click on clear button
		driver.findElement(By.id("clearFilter")).click();
		System.out.println("Close button is clicked!!");

		// Verification*/
	}

	@AfterTest
	public void clear() throws Exception {

		Thread.sleep(5000);
		driver.quit();
	}
}
