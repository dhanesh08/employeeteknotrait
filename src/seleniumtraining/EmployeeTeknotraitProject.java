package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/*
 Test Data:- Project Dropdown
 */
public class EmployeeTeknotraitProject {
	static WebDriver driver;

	@BeforeTest
	public static void login() 
	{

		System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}

	@Test
	public static void testProjectDropdown() throws InterruptedException
	{
		// Click on Employee timesheet button
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]"))
				.click();

		Thread.sleep(5000);

		// Click on Project dropdown
		Select project_dd = new Select(driver.findElement(By.id("filterproject")));
		String project1="Ambrosia";
		project_dd.selectByVisibleText(project1);
		// Verification
		List<WebElement> options = project_dd.getOptions();
		for (WebElement project : options) 
		{
			if (project.getText().contains(project1))
			{
			System.out.println(project.getText());
			}
		}
		// Click on filter button
		driver.findElement(By.id("filter")).click();
		System.out.println("The filter button is clicked!");
		Thread.sleep(4000);

	}

	@AfterTest
	public void logout() throws Exception
	{
		Thread.sleep(4000);
		driver.quit();
	}

}
