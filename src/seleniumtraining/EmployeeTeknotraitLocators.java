package seleniumtraining;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class EmployeeTeknotraitLocators {

	public static void getData(WebDriver driver) throws Exception {

		System.out.println("called !");

		// Click on Employee timesheet button
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]"))
				.click();

		// Enter all the details
		Select user_dd = new Select(driver.findElement(By.id("filteruser")));
		user_dd.selectByVisibleText("Dharmendra Patel");
		Select project_dd = new Select(driver.findElement(By.id("filterproject")));
		project_dd.selectByVisibleText("Ambrosia");
		Select type_dd = new Select(driver.findElement(By.xpath("//select[@class='work_type']")));
		type_dd.selectByVisibleText("Normal Hours");
		Select status_dd = new Select(driver.findElement(By.xpath("//select[@class='approve_reject_status']")));
		status_dd.selectByVisibleText("Approved");

		// Click on filter button
		driver.findElement(By.id("filter")).click();
		System.out.println("The filter button is clicked!");
		Thread.sleep(4000);

		// Click on clear button
		driver.findElement(By.id("clearFilter")).click();
		System.out.println("Close button is clicked!!");

	}

}
