package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
/*
 Test data:- Status
 */
public class EmployeeTeknotraitStatusDropdown {
	static WebDriver driver;
	@BeforeTest
	public static void login() {
		
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}
	@Test
	public static void testStatusDropdown() throws InterruptedException
	{
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]"))
		.click();
		Select status_dd = new Select(driver.findElement(By.xpath("//select[@class='approve_reject_status']")));
		// Verification
		List<WebElement> options=status_dd.getOptions();
		String status1= "Approved";
		Thread.sleep(5000);
		status_dd.selectByVisibleText(status1);
		for (WebElement status:options)
		{
			if (status.getText().contains(status1))
			{
			System.out.println(status.getText());
			}
		}

	}
	@AfterTest
	public static void logout() throws Exception
	{
		Thread.sleep(5000);
		driver.quit();
	}

}
