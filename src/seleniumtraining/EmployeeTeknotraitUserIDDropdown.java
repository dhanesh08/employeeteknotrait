package seleniumtraining;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EmployeeTeknotraitUserIDDropdown {
	static WebDriver driver;
	@BeforeTest
	public static void login()
	{
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\Desktop\\SeleniumTraining\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");

		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
	}
	@Test
	public static void userIDDropdown()
	{
		// Click on Employee timesheet dropdown
		driver.findElement(By.xpath("//a[@class='EmployeeTimesheet']//span[contains(text(),'Employee Timesheet')]")).click();
		// Click on Add Timesheet
		driver.findElement(By.id("add_timesheet")).click();
		// Click on user id dropdown
		String user_id1 = "Dharmendra Patel";
		Select user_id_dd=new Select(driver.findElement(By.id("user_id")));
		user_id_dd.selectByVisibleText(user_id1);
		List <WebElement> options=user_id_dd.getOptions();
		for (WebElement user_id:options)
		{
			if (user_id.getText().contains(user_id1))
			{
				System.out.println(user_id.getText());
			}
		}
	}
	@AfterTest
	public static void quit() throws Exception
	{
		Thread.sleep(3000);
		driver.quit();
	}

}
